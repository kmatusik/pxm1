# serial 61 is connected to the SPiiPlus Motor Controller
#drvAsynIPPortConfigure("portName","hostInfo",priority,noAutoConnect,noProcessEos)
drvAsynIPPortConfigure("SPiiPlus", "10.0.0.57:701", 0, 0, 0)
asynOctetSetInputEos("SPiiPlus",0,"\r")
asynOctetSetOutputEos("SPiiPlus",0,"\r")

dbLoadRecords("$(ASYN)/db/asynRecord.db","P=pxm1:,R=asyn,PORT=SPiiPlus,ADDR=0,OMAX=0,IMAX=0")
dbLoadRecords("$(TOP)/pxm1App/Db/SPiiDrive.db","P=pxm1:,R=spii:,PORT=SPiiPlus,SCAN=8")
