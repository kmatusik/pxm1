# Description:
# digital/binary port substitution file. 
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# Licence as published by the Free Software Foundation; either
# version 2.1 of the Licence, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public Licence for more details.
#
# You should have received a copy of the GNU Lesser General Public
# Licence along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Contact details:
# mark.clift@synchrotron.org.au
# 800 Blackburn Road, Clayton, Victoria 3168, Australia.
#

# P    - PV prefix
# R    - Record Name
# PORT - Asyn port name
# BYTE - Hardware byte to read
# MASK - Mask for this bit

# Digital IO naming
# $(P)$(R)<Byte or word num><Type Bo or Bi><Bit>

file "$(GALIL)/GalilSup/Db/galil_digital_in_bit.template"
{
pattern {P,       R,          PORT,  BYTE,  MASK,     ZNAM,  ONAM,  ZSV,        OSV         }

# DMC = Digital motor controller
# DMC binary inputs are organized in bytes
	{DMC01:,  Galil0Bi0,  Galil1, 0,     0x000001, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil0Bi1,  Galil1, 0,     0x000002, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil0Bi2,  Galil1, 0,     0x000004, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil0Bi3,  Galil1, 0,     0x000008, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil0Bi4,  Galil1, 0,     0x000010, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil0Bi5,  Galil1, 0,     0x000020, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil0Bi6,  Galil1, 0,     0x000040, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil0Bi7,  Galil1, 0,     0x000080, "Off", "On",  "NO_ALARM", "MAJOR"  }

	{DMC01:,  Galil1Bi0,  Galil1, 1,     0x000001, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil1Bi1,  Galil1, 1,     0x000002, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil1Bi2,  Galil1, 1,     0x000004, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil1Bi3,  Galil1, 1,     0x000008, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil1Bi4,  Galil1, 1,     0x000010, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil1Bi5,  Galil1, 1,     0x000020, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil1Bi6,  Galil1, 1,     0x000040, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil1Bi7,  Galil1, 1,     0x000080, "Off", "On",  "NO_ALARM", "MAJOR"  }

	{DMC01:,  Galil2Bi0,  Galil1, 2,     0x000001, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil2Bi1,  Galil1, 2,     0x000002, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil2Bi2,  Galil1, 2,     0x000004, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil2Bi3,  Galil1, 2,     0x000008, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil2Bi4,  Galil1, 2,     0x000010, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil2Bi5,  Galil1, 2,     0x000020, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil2Bi6,  Galil1, 2,     0x000040, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC01:,  Galil2Bi7,  Galil1, 2,     0x000080, "Off", "On",  "NO_ALARM", "MAJOR"  }

	{DMC02:,  Galil0Bi0,  Galil2, 0,     0x000001, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil0Bi1,  Galil2, 0,     0x000002, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil0Bi2,  Galil2, 0,     0x000004, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil0Bi3,  Galil2, 0,     0x000008, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil0Bi4,  Galil2, 0,     0x000010, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil0Bi5,  Galil2, 0,     0x000020, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil0Bi6,  Galil2, 0,     0x000040, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil0Bi7,  Galil2, 0,     0x000080, "Off", "On",  "NO_ALARM", "MAJOR"  }

	{DMC02:,  Galil1Bi0,  Galil2, 1,     0x000001, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil1Bi1,  Galil2, 1,     0x000002, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil1Bi2,  Galil2, 1,     0x000004, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil1Bi3,  Galil2, 1,     0x000008, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil1Bi4,  Galil2, 1,     0x000010, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil1Bi5,  Galil2, 1,     0x000020, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil1Bi6,  Galil2, 1,     0x000040, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil1Bi7,  Galil2, 1,     0x000080, "Off", "On",  "NO_ALARM", "MAJOR"  }

	{DMC02:,  Galil2Bi0,  Galil2, 2,     0x000001, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil2Bi1,  Galil2, 2,     0x000002, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil2Bi2,  Galil2, 2,     0x000004, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil2Bi3,  Galil2, 2,     0x000008, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil2Bi4,  Galil2, 2,     0x000010, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil2Bi5,  Galil2, 2,     0x000020, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil2Bi6,  Galil2, 2,     0x000040, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC02:,  Galil2Bi7,  Galil2, 2,     0x000080, "Off", "On",  "NO_ALARM", "MAJOR"  }

	{DMC03:,  Galil0Bi0,  Galil3, 0,     0x000001, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil0Bi1,  Galil3, 0,     0x000002, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil0Bi2,  Galil3, 0,     0x000004, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil0Bi3,  Galil3, 0,     0x000008, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil0Bi4,  Galil3, 0,     0x000010, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil0Bi5,  Galil3, 0,     0x000020, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil0Bi6,  Galil3, 0,     0x000040, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil0Bi7,  Galil3, 0,     0x000080, "Off", "On",  "NO_ALARM", "MAJOR"  }

	{DMC03:,  Galil1Bi0,  Galil3, 1,     0x000001, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil1Bi1,  Galil3, 1,     0x000002, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil1Bi2,  Galil3, 1,     0x000004, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil1Bi3,  Galil3, 1,     0x000008, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil1Bi4,  Galil3, 1,     0x000010, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil1Bi5,  Galil3, 1,     0x000020, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil1Bi6,  Galil3, 1,     0x000040, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil1Bi7,  Galil3, 1,     0x000080, "Off", "On",  "NO_ALARM", "MAJOR"  }

	{DMC03:,  Galil2Bi0,  Galil3, 2,     0x000001, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil2Bi1,  Galil3, 2,     0x000002, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil2Bi2,  Galil3, 2,     0x000004, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil2Bi3,  Galil3, 2,     0x000008, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil2Bi4,  Galil3, 2,     0x000010, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil2Bi5,  Galil3, 2,     0x000020, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil2Bi6,  Galil3, 2,     0x000040, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC03:,  Galil2Bi7,  Galil3, 2,     0x000080, "Off", "On",  "NO_ALARM", "MAJOR"  }

	{DMC04:,  Galil0Bi0,  Galil4, 0,     0x000001, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil0Bi1,  Galil4, 0,     0x000002, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil0Bi2,  Galil4, 0,     0x000004, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil0Bi3,  Galil4, 0,     0x000008, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil0Bi4,  Galil4, 0,     0x000010, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil0Bi5,  Galil4, 0,     0x000020, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil0Bi6,  Galil4, 0,     0x000040, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil0Bi7,  Galil4, 0,     0x000080, "Off", "On",  "NO_ALARM", "MAJOR"  }

	{DMC04:,  Galil1Bi0,  Galil4, 1,     0x000001, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil1Bi1,  Galil4, 1,     0x000002, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil1Bi2,  Galil4, 1,     0x000004, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil1Bi3,  Galil4, 1,     0x000008, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil1Bi4,  Galil4, 1,     0x000010, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil1Bi5,  Galil4, 1,     0x000020, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil1Bi6,  Galil4, 1,     0x000040, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil1Bi7,  Galil4, 1,     0x000080, "Off", "On",  "NO_ALARM", "MAJOR"  }

	{DMC04:,  Galil2Bi0,  Galil4, 2,     0x000001, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil2Bi1,  Galil4, 2,     0x000002, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil2Bi2,  Galil4, 2,     0x000004, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil2Bi3,  Galil4, 2,     0x000008, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil2Bi4,  Galil4, 2,     0x000010, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil2Bi5,  Galil4, 2,     0x000020, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil2Bi6,  Galil4, 2,     0x000040, "Off", "On",  "NO_ALARM", "MAJOR"  }
	{DMC04:,  Galil2Bi7,  Galil4, 2,     0x000080, "Off", "On",  "NO_ALARM", "MAJOR"  }
}

# P    - PV prefix
# R    - Record Name
# PORT - Asyn port name
# WORD - Hardware word to read
# MASK - Mask for this bit

file "$(GALIL)/GalilSup/Db/galil_digital_out_bit.template"
{
pattern {P,       R,           PORT,  WORD,  MASK,     ZNAM,  ONAM,  ZSV,        OSV         }

# DMC binary outputs are organized in 16bit words
	{DMC01:,  Galil0Bo0,   Galil1, 0,     0x000001, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo1,   Galil1, 0,     0x000002, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo2,   Galil1, 0,     0x000004, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo3,   Galil1, 0,     0x000008, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo4,   Galil1, 0,     0x000010, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo5,   Galil1, 0,     0x000020, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo6,   Galil1, 0,     0x000040, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo7,   Galil1, 0,     0x000080, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo8,   Galil1, 0,     0x000100, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo9,   Galil1, 0,     0x000200, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo10,  Galil1, 0,     0x000400, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo11,  Galil1, 0,     0x000800, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo12,  Galil1, 0,     0x001000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo13,  Galil1, 0,     0x002000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo14,  Galil1, 0,     0x004000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil0Bo15,  Galil1, 0,     0x008000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }

	{DMC01:,  Galil1Bo0,   Galil1, 1,     0x000001, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo1,   Galil1, 1,     0x000002, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo2,   Galil1, 1,     0x000004, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo3,   Galil1, 1,     0x000008, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo4,   Galil1, 1,     0x000010, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo5,   Galil1, 1,     0x000020, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo6,   Galil1, 1,     0x000040, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo7,   Galil1, 1,     0x000080, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo8,   Galil1, 1,     0x000100, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo9,   Galil1, 1,     0x000200, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo10,  Galil1, 1,     0x000400, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo11,  Galil1, 1,     0x000800, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo12,  Galil1, 1,     0x001000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo13,  Galil1, 1,     0x002000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo14,  Galil1, 1,     0x004000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC01:,  Galil1Bo15,  Galil1, 1,     0x008000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }

	{DMC02:,  Galil0Bo0,   Galil2 0,     0x000001, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo1,   Galil2 0,     0x000002, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo2,   Galil2 0,     0x000004, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo3,   Galil2 0,     0x000008, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo4,   Galil2 0,     0x000010, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo5,   Galil2 0,     0x000020, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo6,   Galil2 0,     0x000040, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo7,   Galil2 0,     0x000080, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo8,   Galil2 0,     0x000100, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo9,   Galil2 0,     0x000200, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo10,  Galil2 0,     0x000400, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo11,  Galil2 0,     0x000800, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo12,  Galil2 0,     0x001000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo13,  Galil2 0,     0x002000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo14,  Galil2 0,     0x004000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil0Bo15,  Galil2 0,     0x008000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }

	{DMC02:,  Galil1Bo0,   Galil2 1,     0x000001, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo1,   Galil2 1,     0x000002, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo2,   Galil2 1,     0x000004, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo3,   Galil2 1,     0x000008, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo4,   Galil2 1,     0x000010, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo5,   Galil2 1,     0x000020, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo6,   Galil2 1,     0x000040, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo7,   Galil2 1,     0x000080, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo8,   Galil2 1,     0x000100, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo9,   Galil2 1,     0x000200, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo10,  Galil2 1,     0x000400, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo11,  Galil2 1,     0x000800, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo12,  Galil2 1,     0x001000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo13,  Galil2 1,     0x002000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo14,  Galil2 1,     0x004000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC02:,  Galil1Bo15,  Galil2 1,     0x008000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }

	{DMC03:,  Galil0Bo0,   Galil3, 0,     0x000001, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo1,   Galil3, 0,     0x000002, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo2,   Galil3, 0,     0x000004, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo3,   Galil3, 0,     0x000008, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo4,   Galil3, 0,     0x000010, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo5,   Galil3, 0,     0x000020, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo6,   Galil3, 0,     0x000040, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo7,   Galil3, 0,     0x000080, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo8,   Galil3, 0,     0x000100, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo9,   Galil3, 0,     0x000200, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo10,  Galil3, 0,     0x000400, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo11,  Galil3, 0,     0x000800, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo12,  Galil3, 0,     0x001000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo13,  Galil3, 0,     0x002000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo14,  Galil3, 0,     0x004000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil0Bo15,  Galil3, 0,     0x008000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }

	{DMC03:,  Galil1Bo0,   Galil3, 1,     0x000001, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo1,   Galil3, 1,     0x000002, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo2,   Galil3, 1,     0x000004, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo3,   Galil3, 1,     0x000008, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo4,   Galil3, 1,     0x000010, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo5,   Galil3, 1,     0x000020, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo6,   Galil3, 1,     0x000040, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo7,   Galil3, 1,     0x000080, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo8,   Galil3, 1,     0x000100, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo9,   Galil3, 1,     0x000200, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo10,  Galil3, 1,     0x000400, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo11,  Galil3, 1,     0x000800, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo12,  Galil3, 1,     0x001000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo13,  Galil3, 1,     0x002000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo14,  Galil3, 1,     0x004000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC03:,  Galil1Bo15,  Galil3, 1,     0x008000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }

	{DMC04:,  Galil0Bo0,   Galil4, 0,     0x000001, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo1,   Galil4, 0,     0x000002, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo2,   Galil4, 0,     0x000004, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo3,   Galil4, 0,     0x000008, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo4,   Galil4, 0,     0x000010, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo5,   Galil4, 0,     0x000020, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo6,   Galil4, 0,     0x000040, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo7,   Galil4, 0,     0x000080, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo8,   Galil4, 0,     0x000100, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo9,   Galil4, 0,     0x000200, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo10,  Galil4, 0,     0x000400, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo11,  Galil4, 0,     0x000800, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo12,  Galil4, 0,     0x001000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo13,  Galil4, 0,     0x002000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo14,  Galil4, 0,     0x004000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil0Bo15,  Galil4, 0,     0x008000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }

	{DMC04:,  Galil1Bo0,   Galil4, 1,     0x000001, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo1,   Galil4, 1,     0x000002, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo2,   Galil4, 1,     0x000004, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo3,   Galil4, 1,     0x000008, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo4,   Galil4, 1,     0x000010, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo5,   Galil4, 1,     0x000020, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo6,   Galil4, 1,     0x000040, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo7,   Galil4, 1,     0x000080, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo8,   Galil4, 1,     0x000100, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo9,   Galil4, 1,     0x000200, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo10,  Galil4, 1,     0x000400, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo11,  Galil4, 1,     0x000800, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo12,  Galil4, 1,     0x001000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo13,  Galil4, 1,     0x002000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo14,  Galil4, 1,     0x004000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
	{DMC04:,  Galil1Bo15,  Galil4, 1,     0x008000, "Off", "On",  "NO_ALARM", "NO_ALARM"  }
}

# end
