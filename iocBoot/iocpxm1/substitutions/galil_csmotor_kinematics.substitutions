# Description:
# CS Motor extras substitution file. 
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# Licence as published by the Free Software Foundation; either
# version 2.1 of the Licence, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public Licence for more details.
#
# You should have received a copy of the GNU Lesser General Public
# Licence along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Contact details:
# mark.clift@synchrotron.org.au
# 800 Blackburn Road, Clayton, Victoria 3168, Australia.

# Forward kinematic transform equations for CS motors
#
# 8 forward equations per controller.
# 1 forward equation per CS motor I-P (8-15) = 8 in total
#
# Eg. I=(A+B)/2 - Forward equations for CS motor I (8)
#
# P    - PV prefix
# M    - CSMotor name
# PORT - Asyn port name
# ADDR - CS Motor I-P (8-15)

file "$(GALIL)/GalilSup/Db/galil_forward_transform.template"
{
   pattern { P,            M,    PORT,    ADDR }
# CS motors (forward transforms)
           { "DMC01:",     "I",  "Galil1", "8"   }
           { "DMC01:",     "J",  "Galil1", "9"   }
           { "DMC01:",     "K",  "Galil1", "10"  }
           { "DMC01:",     "L",  "Galil1", "11"  }
           { "DMC01:",     "M",  "Galil1", "12"  }
           { "DMC01:",     "N",  "Galil1", "13"  }
           { "DMC01:",     "O",  "Galil1", "14"  }
           { "DMC01:",     "P",  "Galil1", "15"  }

           { "DMC02:",     "I",  "Galil2", "8"   }
           { "DMC02:",     "J",  "Galil2", "9"   }
           { "DMC02:",     "K",  "Galil2", "10"  }
           { "DMC02:",     "L",  "Galil2", "11"  }
           { "DMC02:",     "M",  "Galil2", "12"  }
           { "DMC02:",     "N",  "Galil2", "13"  }
           { "DMC02:",     "O",  "Galil2", "14"  }
           { "DMC02:",     "P",  "Galil2", "15"  }

           { "DMC03:",     "I",  "Galil3", "8"   }
           { "DMC03:",     "J",  "Galil3", "9"   }
           { "DMC03:",     "K",  "Galil3", "10"  }
           { "DMC03:",     "L",  "Galil3", "11"  }
           { "DMC03:",     "M",  "Galil3", "12"  }
           { "DMC03:",     "N",  "Galil3", "13"  }
           { "DMC03:",     "O",  "Galil3", "14"  }
           { "DMC03:",     "P",  "Galil3", "15"  }

           { "DMC04:",     "I",  "Galil4", "8"   }
           { "DMC04:",     "J",  "Galil4", "9"   }
           { "DMC04:",     "K",  "Galil4", "10"  }
           { "DMC04:",     "L",  "Galil4", "11"  }
           { "DMC04:",     "M",  "Galil4", "12"  }
           { "DMC04:",     "N",  "Galil4", "13"  }
           { "DMC04:",     "O",  "Galil4", "14"  }
           { "DMC04:",     "P",  "Galil4", "15"  }
}

# Reverse kinematic transform equations for CS motors
#
# 8 reverse equations per CS Motor that represent real motors A-H
# 64 reverse equations per controller in total as there are 8 CS motors I-P (8-15)
#
# Eg. A=I-J/2 - Reverse equation A for CS motor I (8)
#
# P    - PV prefix
# M    - CSMotor name
# PORT - Asyn port name
# ADDR - CS Motor I-P (8-15)

file "$(GALIL)/GalilSup/Db/galil_reverse_transforms.template"
{
   pattern { P,            M,    PORT,    ADDR }
# CS motors (reverse transforms)
           { "DMC01:",     "I",  "Galil1", "8"   }
           { "DMC01:",     "J",  "Galil1", "9"   }
           { "DMC01:",     "K",  "Galil1", "10"  }
           { "DMC01:",     "L",  "Galil1", "11"  }
           { "DMC01:",     "M",  "Galil1", "12"  }
           { "DMC01:",     "N",  "Galil1", "13"  }
           { "DMC01:",     "O",  "Galil1", "14"  }
           { "DMC01:",     "P",  "Galil1", "15"  }

           { "DMC02:",     "I",  "Galil2", "8"   }
           { "DMC02:",     "J",  "Galil2", "9"   }
           { "DMC02:",     "K",  "Galil2", "10"  }
           { "DMC02:",     "L",  "Galil2", "11"  }
           { "DMC02:",     "M",  "Galil2", "12"  }
           { "DMC02:",     "N",  "Galil2", "13"  }
           { "DMC02:",     "O",  "Galil2", "14"  }
           { "DMC02:",     "P",  "Galil2", "15"  }

           { "DMC03:",     "I",  "Galil3", "8"   }
           { "DMC03:",     "J",  "Galil3", "9"   }
           { "DMC03:",     "K",  "Galil3", "10"  }
           { "DMC03:",     "L",  "Galil3", "11"  }
           { "DMC03:",     "M",  "Galil3", "12"  }
           { "DMC03:",     "N",  "Galil3", "13"  }
           { "DMC03:",     "O",  "Galil3", "14"  }
           { "DMC03:",     "P",  "Galil3", "15"  }

           { "DMC04:",     "I",  "Galil4", "8"   }
           { "DMC04:",     "J",  "Galil4", "9"   }
           { "DMC04:",     "K",  "Galil4", "10"  }
           { "DMC04:",     "L",  "Galil4", "11"  }
           { "DMC04:",     "M",  "Galil4", "12"  }
           { "DMC04:",     "N",  "Galil4", "13"  }
           { "DMC04:",     "O",  "Galil4", "14"  }
           { "DMC04:",     "P",  "Galil4", "15"  }
}

# end

