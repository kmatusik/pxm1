# Description:
# adc/dac substitution file. 
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# Licence as published by the Free Software Foundation; either
# version 2.1 of the Licence, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public Licence for more details.
#
# You should have received a copy of the GNU Lesser General Public
# Licence along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Contact details:
# mark.clift@synchrotron.org.au
# 800 Blackburn Road, Clayton, Victoria 3168, Australia.
#
# P    - PV prefix
# R    - Record Name
# PORT - Asyn port name
# ADDR - Hardware port to read
# PREC - Precision

file "$(GALIL)/GalilSup/Db/galil_analog_in.template"
{ 
   pattern { P,           R,           PORT,    ADDR,  SCAN,       PREC }

#DMC Ports numbered 0 to 7 at database layer for GUI.
#DMC Ports numbered 1 to 8 on controller hardware
	   { "DMC01:",    "GalilAi0",  "Galil1", "1",   "I/O Intr", "3"  }
	   { "DMC01:",    "GalilAi1",  "Galil1", "2",   "I/O Intr", "3"  }
	   { "DMC01:",    "GalilAi2",  "Galil1", "3",   "I/O Intr", "3"  }
	   { "DMC01:",    "GalilAi3",  "Galil1", "4",   "I/O Intr", "3"  }
	   { "DMC01:",    "GalilAi4",  "Galil1", "5",   "I/O Intr", "3"  }
	   { "DMC01:",    "GalilAi5",  "Galil1", "6",   "I/O Intr", "3"  }
	   { "DMC01:",    "GalilAi6",  "Galil1", "7",   "I/O Intr", "3"  }
	   { "DMC01:",    "GalilAi7",  "Galil1", "8",   "I/O Intr", "3"  }

	   { "DMC02:",    "GalilAi0",  "Galil2", "1",   "I/O Intr", "3"  }
	   { "DMC02:",    "GalilAi1",  "Galil2", "2",   "I/O Intr", "3"  }
	   { "DMC02:",    "GalilAi2",  "Galil2", "3",   "I/O Intr", "3"  }
	   { "DMC02:",    "GalilAi3",  "Galil2", "4",   "I/O Intr", "3"  }
	   { "DMC02:",    "GalilAi4",  "Galil2", "5",   "I/O Intr", "3"  }
	   { "DMC02:",    "GalilAi5",  "Galil2", "6",   "I/O Intr", "3"  }
	   { "DMC02:",    "GalilAi6",  "Galil2", "7",   "I/O Intr", "3"  }
	   { "DMC02:",    "GalilAi7",  "Galil2", "8",   "I/O Intr", "3"  }
	   
	   { "DMC03:",    "GalilAi0",  "Galil3", "1",   "I/O Intr", "3"  }
	   { "DMC03:",    "GalilAi1",  "Galil3", "2",   "I/O Intr", "3"  }
	   { "DMC03:",    "GalilAi2",  "Galil3", "3",   "I/O Intr", "3"  }
	   { "DMC03:",    "GalilAi3",  "Galil3", "4",   "I/O Intr", "3"  }
	   { "DMC03:",    "GalilAi4",  "Galil3", "5",   "I/O Intr", "3"  }
	   { "DMC03:",    "GalilAi5",  "Galil3", "6",   "I/O Intr", "3"  }
	   { "DMC03:",    "GalilAi6",  "Galil3", "7",   "I/O Intr", "3"  }
	   { "DMC03:",    "GalilAi7",  "Galil3", "8",   "I/O Intr", "3"  }
	   
	   { "DMC04:",    "GalilAi0",  "Galil4", "1",   "I/O Intr", "3"  }
	   { "DMC04:",    "GalilAi1",  "Galil4", "2",   "I/O Intr", "3"  }
	   { "DMC04:",    "GalilAi2",  "Galil4", "3",   "I/O Intr", "3"  }
	   { "DMC04:",    "GalilAi3",  "Galil4", "4",   "I/O Intr", "3"  }
	   { "DMC04:",    "GalilAi4",  "Galil4", "5",   "I/O Intr", "3"  }
	   { "DMC04:",    "GalilAi5",  "Galil4", "6",   "I/O Intr", "3"  }
	   { "DMC04:",    "GalilAi6",  "Galil4", "7",   "I/O Intr", "3"  }
	   { "DMC04:",    "GalilAi7",  "Galil4", "8",   "I/O Intr", "3"  }

}

file "$(GALIL)/GalilSup/Db/galil_analog_out.template"
{ 
   pattern { P,           R,           PORT,    ADDR,  PREC,  LOPR,  HOPR }

#DMC Ports numbered 0 to 7 at database layer for GUI.
#DMC Ports numbered 1 to 8 on controller hardware
	   { "DMC01:",    "GalilAo0",  "Galil1", "1",   "3",   "-10", "10" }
	   { "DMC01:",    "GalilAo1",  "Galil1", "2",   "3",   "-10", "10" }
	   { "DMC01:",    "GalilAo2",  "Galil1", "3",   "3",   "-10", "10" }
	   { "DMC01:",    "GalilAo3",  "Galil1", "4",   "3",   "-10", "10" }
	   { "DMC01:",    "GalilAo4",  "Galil1", "5",   "3",   "-10", "10" }
	   { "DMC01:",    "GalilAo5",  "Galil1", "6",   "3",   "-10", "10" }
	   { "DMC01:",    "GalilAo6",  "Galil1", "7",   "3",   "-10", "10" }
	   { "DMC01:",    "GalilAo7",  "Galil1", "8",   "3",   "-10", "10" }

	   { "DMC02:",    "GalilAo0",  "Galil2", "1",   "3",   "-10", "10" }
	   { "DMC02:",    "GalilAo1",  "Galil2", "2",   "3",   "-10", "10" }
	   { "DMC02:",    "GalilAo2",  "Galil2", "3",   "3",   "-10", "10" }
	   { "DMC02:",    "GalilAo3",  "Galil2", "4",   "3",   "-10", "10" }
	   { "DMC02:",    "GalilAo4",  "Galil2", "5",   "3",   "-10", "10" }
	   { "DMC02:",    "GalilAo5",  "Galil2", "6",   "3",   "-10", "10" }
	   { "DMC02:",    "GalilAo6",  "Galil2", "7",   "3",   "-10", "10" }
	   { "DMC02:",    "GalilAo7",  "Galil2", "8",   "3",   "-10", "10" }

	   { "DMC03:",    "GalilAo0",  "Galil3", "1",   "3",   "-10", "10" }
	   { "DMC03:",    "GalilAo1",  "Galil3", "2",   "3",   "-10", "10" }
	   { "DMC03:",    "GalilAo2",  "Galil3", "3",   "3",   "-10", "10" }
	   { "DMC03:",    "GalilAo3",  "Galil3", "4",   "3",   "-10", "10" }
	   { "DMC03:",    "GalilAo4",  "Galil3", "5",   "3",   "-10", "10" }
	   { "DMC03:",    "GalilAo5",  "Galil3", "6",   "3",   "-10", "10" }
	   { "DMC03:",    "GalilAo6",  "Galil3", "7",   "3",   "-10", "10" }
	   { "DMC03:",    "GalilAo7",  "Galil3", "8",   "3",   "-10", "10" }

	   { "DMC04:",    "GalilAo0",  "Galil4", "1",   "3",   "-10", "10" }
	   { "DMC04:",    "GalilAo1",  "Galil4", "2",   "3",   "-10", "10" }
	   { "DMC04:",    "GalilAo2",  "Galil4", "3",   "3",   "-10", "10" }
	   { "DMC04:",    "GalilAo3",  "Galil4", "4",   "3",   "-10", "10" }
	   { "DMC04:",    "GalilAo4",  "Galil4", "5",   "3",   "-10", "10" }
	   { "DMC04:",    "GalilAo5",  "Galil4", "6",   "3",   "-10", "10" }
	   { "DMC04:",    "GalilAo6",  "Galil4", "7",   "3",   "-10", "10" }
	   { "DMC04:",    "GalilAo7",  "Galil4", "8",   "3",   "-10", "10" }

}

# end

