
# SRS 570 Current amplifier
drvAsynIPPortConfigure("SRS0", "10.0.0.144:4001",0,0,0)
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=pxm1:,R=srs570:asyn,PORT=SRS0,ADDR=0,IMAX=100,OMAX=100")
#asynSetTraceMask("DMM0",-1,0xb)
#asynSetTraceIOMask("DMM0",-1,0x1)
iocshLoad("$(IP)/iocsh/SR_570.iocsh", "PREFIX=pxm1:, INSTANCE=SR570:, PORT=SRS0")
