# Linux startup script

< envPaths

# Increase size of buffer for error logging from default 1256
errlogInit(20000)

################################################################################
# Tell EPICS all about the record types, device-support modules, drivers,
# etc. in the software we just loaded (pxm1.munch)
dbLoadDatabase("../../dbd/iocpxm1Linux.dbd")
iocpxm1Linux_registerRecordDeviceDriver(pdbbase)

< common.iocsh

# Galils
< galil.cmd
dbLoadRecords("$(MOTOR)/db/motorUtil.db","P=pxm1:")

# PI air bearing stage
< SPiiPlus.cmd

# PI piezo stage
< PI_GCS2.cmd

# Meerstetter peltier controllers
< meer.iocsh

# Keithley thermocouple monitor
#< keithley.cmd

# Dithering related pvs
#< dithering.iocsh

# Filter wheel pvs
< filter_wheel.iocsh

# devIocStats
dbLoadRecords("$(DEVIOCSTATS)/db/iocAdminSoft.db","IOC=pxm1")

###############################################################################
iocInit
###############################################################################
#Set some motor attributes that do not seem to hold on IOC boot

#dbpf("DMC01:SEND_STR_CMD", "YAC=2")


# write all the PV names to a local file
dbl > dbl-all.txt

# Diagnostic: CA links in all records
dbcar(0,1)

# print the time our boot was finished
date
