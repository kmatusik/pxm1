# PI GCS2 support

dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/PI_GCS2.substitutions")

drvAsynIPPortConfigure("E754_ETH","10.0.0.150:50000",0,0,0)

# configure terminators
asynOctetSetInputEos("E754_ETH",0,"\n")
asynOctetSetOutputEos("E754_ETH",0,"\n")

# Turn on asyn trace
#asynSetTraceMask("E754_ETH",0,3)
#asynSetTraceIOMask("E754_ETH",0,1)

# PI_GCS2_CreateController(portName, asynPort, numAxes, priority, stackSize, movingPollingRate, idlePollingRate)
PI_GCS2_CreateController("E754", "E754_ETH",1, 0,0, 100, 1000)

# Turn off asyn trace
#asynSetTraceMask("E754_ETH",0,1)
#asynSetTraceIOMask("E754_ETH",0,0)

#!dbLoadRecords("$(TOP)/pxm1App/Db/PI_Support.db","P=pxm1:,R=p1:,PORT=E754_ETH,ADDR=0,TIMEOUT=1") 

# asyn record for troubleshooting
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=pxm1:,R=asyn,PORT=E754_ETH,ADDR=0,OMAX=256,IMAX=256")
