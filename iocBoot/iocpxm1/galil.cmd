## uncomment to see every command sent to galil
#epicsEnvSet("GALIL_DEBUG_FILE", "galil_debug.txt")

#Load motor records for real and coordinate system (CS) motors
dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/galil_motors-v6-10up.substitutions")

#Load DMC controller features (eg.  Limit switch type, home switch type, output compare, message consoles)
dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/galil_dmc_ctrl.substitutions")

#Load extra features for real axis/motors (eg. Motor type, encoder type)
dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/galil_motor_extras.substitutions")

#Load extra features for CS axis/motors (eg. Setpoint monitor)
dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/galil_csmotor_extras.substitutions")

#Load kinematics for CS axis/motors (eg. Forward and reverse kinematics, kinematic variables)
dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/galil_csmotor_kinematics.substitutions")

#Load coordinate system features (eg. Coordinate system S and T status, motor list, segments processed, moving status)
dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/galil_coordinate_systems.substitutions")

#Load digital IO databases
dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/galil_digital_ports.substitutions")

#Load analog IO databases
dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/galil_analog_ports.substitutions")

#Load user defined functions
#dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/galil_userdef_records.substitutions")

#Load user defined array support
#dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/galil_user_array.substitutions")

#Load profiles
dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/galil_profileMoveController.substitutions")
dbLoadTemplate("$(TOP)/iocBoot/iocpxm1/substitutions/galil_profileMoveAxis.substitutions")

# GalilCreateController command parameters are:
#
# 1. Const char *portName 	- The name of the asyn port that will be created for this controller
# 2. Const char *address 	- The address of the controller
# 3. double updatePeriod	- The time in ms between datarecords 2ms min, 200ms max.  Async if controller + bus supports it, otherwise is polled/synchronous.
#                       	- Recommend 50ms or less for ethernet
#                       	- Specify negative updatePeriod < 0 to force synchronous tcp poll period.  Otherwise will try async udp mode first

# Create a Galil controller
GalilCreateController("Galil1", "10.0.0.10", 8)
GalilCreateController("Galil2", "10.0.0.11", 8)
GalilCreateController("Galil3", "10.0.0.12", 8)
GalilCreateController("Galil4", "10.0.0.13", 8)

# GalilCreateAxis command parameters are:
#
# 1. char *portName Asyn port for controller
# 2. char  axis A-H,
# 3. int   limits_as_home (0 off 1 on), 
# 4. char  *Motor interlock digital port number 1 to 8 eg. "1,2,4".  1st 8 bits are supported
# 5. int   Interlock switch type 0 normally open, all other values is normally closed interlock switch type

# Create the axis
GalilCreateAxis("Galil1","A",1,"",0)
GalilCreateAxis("Galil1","B",1,"",0)
GalilCreateAxis("Galil1","C",1,"",0)
GalilCreateAxis("Galil1","D",1,"",0)
GalilCreateAxis("Galil1","E",1,"",0)
GalilCreateAxis("Galil1","F",1,"",0)
GalilCreateAxis("Galil1","G",1,"",0)
GalilCreateAxis("Galil1","H",1,"",0)
GalilCreateAxis("Galil2","A",1,"",0)
GalilCreateAxis("Galil2","B",1,"",0)
GalilCreateAxis("Galil2","C",1,"",0)
GalilCreateAxis("Galil2","D",1,"",0)
GalilCreateAxis("Galil2","E",1,"",0)
GalilCreateAxis("Galil2","F",1,"",0)
GalilCreateAxis("Galil2","G",1,"",0)
GalilCreateAxis("Galil2","H",1,"",0)
GalilCreateAxis("Galil3","A",1,"",1)
GalilCreateAxis("Galil3","B",1,"",1)
GalilCreateAxis("Galil3","C",1,"",1)
GalilCreateAxis("Galil3","D",1,"",1)
GalilCreateAxis("Galil3","E",1,"",1)
GalilCreateAxis("Galil3","F",1,"",1)
GalilCreateAxis("Galil3","G",1,"",1)
GalilCreateAxis("Galil3","H",1,"",1)
GalilCreateAxis("Galil4","A",1,"",1)
GalilCreateAxis("Galil4","B",1,"",1)
GalilCreateAxis("Galil4","C",1,"",1)
GalilCreateAxis("Galil4","D",1,"",1)
GalilCreateAxis("Galil4","E",1,"",1)
GalilCreateAxis("Galil4","F",1,"",1)
GalilCreateAxis("Galil4","G",1,"",1)
GalilCreateAxis("Galil4","H",1,"",1)

# GalilCreateCSAxes command parameters are:
#
# 1. char *portName Asyn port for controller

#Create all CS axes (ie. I-P axis)
GalilCreateCSAxes("Galil1")
GalilCreateCSAxes("Galil2")
GalilCreateCSAxes("Galil3")
GalilCreateCSAxes("Galil4")

# GalilStartController command parameters are:
#
# 1. char *portName Asyn port for controller
# 2. char *code file(s) to deliver to the controller we are starting. "" = use generated code (recommended)
#             Specify a single file or to use templates use: headerfile;bodyfile1!bodyfile2!bodyfileN;footerfile
# 3. int   Burn program to EEPROM conditions
#             0 = transfer code if differs from eeprom, dont burn code to eeprom, then finally execute code thread 0
#             1 = transfer code if differs from eeprom, burn code to eeprom, then finally execute code thread 0
#             It is asssumed thread 0 starts all other required threads
# 4. int   Thread mask.  Check these threads are running after controller code start.  Bit 0 = thread 0 and so on
#             if thread mask < 0 nothing is checked
#             if thread mask = 0 and GalilCreateAxis appears > 0 then threads 0 to number of GalilCreateAxis is checked (good when using the generated code)

# Start the controller
GalilStartController("Galil1", "", 1, 0)
GalilStartController("Galil2", "", 1, 0)
GalilStartController("Galil3", "", 1, 0)
GalilStartController("Galil4", "", 1, 0)

# GalilCreateProfile command parameters are:
#
# 1. char *portName Asyn port for controller
# 2. Int maxPoints in trajectory

# Create trajectory profiles
GalilCreateProfile("Galil1", 2000)
GalilCreateProfile("Galil2", 2000)
GalilCreateProfile("Galil3", 2000)
GalilCreateProfile("Galil4", 2000)

