# Keithley 2000 DMM
drvAsynIPPortConfigure("DMM0", "10.0.0.136:1394",0,0,0)
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=pxm1:,R=dmm:D1:asyn,PORT=DMM0,ADDR=0,IMAX=100,OMAX=100")
#asynSetTraceMask("DMM0",-1,0xb)
#asynSetTraceIOMask("DMM0",-1,0x1)
iocshLoad("$(IP)/iocsh/Keithley_2k_gpib.iocsh", "PREFIX=pxm1:dmm:, INSTANCE=D1:, PORT=DMM0, NUM_CHANNELS=20, MODEL=2700, ADDR=0")
# Put Keithley into terse response mode
doAfterIocInit('dbpf("pxm1:dmm:D1:general_input.VAL PP NMS", "form:elem read")')
