#include <stdio.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <string.h>

static long posToStr(aSubRecord *prec) {
	double filtSel_d = *((double *)prec->a);
	int filtSel = (int)filtSel_d;
	switch (filtSel){
		case 0:
			strncpy((char *)prec->vala, "None", sizeof(prec->vala));
			break;
		case 1:
			strncpy((char *)prec->vala, "LE1", sizeof(prec->vala));
			break;
		case 2:
			strncpy((char *)prec->vala, "LE2", sizeof(prec->vala));
			break;
		case 3:
			strncpy((char *)prec->vala, "LE3", sizeof(prec->vala));
			break;
		case 4:
			strncpy((char *)prec->vala, "LE4", sizeof(prec->vala));
			break;
		case 5:
			strncpy((char *)prec->vala, "LE5", sizeof(prec->vala));
			break;
		case 6:
			strncpy((char *)prec->vala, "LE6", sizeof(prec->vala));
			break;
		case 7:
			strncpy((char *)prec->vala, "ME1", sizeof(prec->vala));
			break;
		case 8:
			strncpy((char *)prec->vala, "ME2", sizeof(prec->vala));
			break;
		case 9:
			strncpy((char *)prec->vala, "ME3", sizeof(prec->vala));
			break;
		case 10:
			strncpy((char *)prec->vala, "ME4", sizeof(prec->vala));
			break;
		case 11:
			strncpy((char *)prec->vala, "ME5", sizeof(prec->vala));
			break;
		case 12:
			strncpy((char *)prec->vala, "HE1", sizeof(prec->vala));
			break;
		case 13:
			strncpy((char *)prec->vala, "HE2", sizeof(prec->vala));
			break;
		case 14:
			strncpy((char *)prec->vala, "HE3", sizeof(prec->vala));
			break;
		case 15:
			strncpy((char *)prec->vala, "HE4", sizeof(prec->vala));
			break;
		case 16:
			strncpy((char *)prec->vala, "HE5", sizeof(prec->vala));
			break;
		case 17:
			strncpy((char *)prec->vala, "HE6", sizeof(prec->vala));
			break;
		case 18:
			strncpy((char *)prec->vala, "HE7", sizeof(prec->vala));
			break;
		case 19:
			strncpy((char *)prec->vala, "HE8", sizeof(prec->vala));
			break;
		default:
			strncpy((char *)prec->vala, "Indeterminate", sizeof(prec->vala));
			break;
	}
	return 0;
}


/* Note the function must be registered at the end!*/
epicsRegisterFunction(posToStr);
