#include <stdio.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <string.h>

static long filterIncrement(aSubRecord *prec) {
	double filtSel_d = *((double *)prec->a);
	int filtSel = (int)filtSel_d;

	switch (filtSel){
		case 0:
			*((double *)prec->vala) = 0.0;
			break;
		case 1:
			*((double *)prec->vala) = 85.7;
			break;
		case 2:
			*((double *)prec->vala) = 102.84;
			break;
		case 3:
			*((double *)prec->vala) = 17.14;
			break;
		case 4:
			*((double *)prec->vala) = 34.28;
			break;
		case 5:
			*((double *)prec->vala) = 51.42;
			break;
		case 6:
			*((double *)prec->vala) = 68.56;
			break;
		case 7:
			*((double *)prec->vala) = 342.8;
			break;
		case 8:
			*((double *)prec->vala) = 325.66;
			break;
		case 9:
			*((double *)prec->vala) = 308.52;
			break;
		case 10:
			*((double *)prec->vala) = 291.38;
			break;
		case 11:
			*((double *)prec->vala) = 274.24;
			break;
		case 12:
			*((double *)prec->vala) = 119.98;
			break;
		case 13:
			*((double *)prec->vala) = 137.12;
			break;
		case 14:
			*((double *)prec->vala) = 154.26;
			break;
		case 15:
			*((double *)prec->vala) = 171.4;
			break;
		case 16:
			*((double *)prec->vala) = 188.54;
			break;
		case 17:
			*((double *)prec->vala) = 205.68;
			break;
		case 18:
			*((double *)prec->vala) = 222.82;
			break;
		case 19:
			*((double *)prec->vala) = 239.96;
			break;
		default:
			printf("Indeterminate filt pos\n");
	}
	return 0;
}


/* Note the function must be registered at the end!*/
epicsRegisterFunction(filterIncrement);
